FROM node:13-alpine

WORKDIR /app

# Set common env vars
ENV NODE_ENV production
ENV HOST 0.0.0.0
ENV PORT 3333
ENV NODE_ENV development
ENV APP_URL http://127.0.0.1:3333
ENV CACHE_VIEWS false
ENV APP_KEY 6RhkwxH9ZVLqGdBkD3FunOxgtjOeKoX9
ENV DB_CONNECTION mysql
ENV DB_HOST mysql
ENV DB_PORT 3306
ENV DB_USER root
ENV DB_PASSWORD root
ENV DB_DATABASE ivl
ENV SESSION_DRIVER cookie
ENV HASH_DRIVER bcrypt
ENV ENV_SILENT true

# Install updates and dependencies
# RUN apk add python make g++

# Copy application code.
COPY . /app/

# Install dependencies.
RUN yarn

# start
CMD ["yarn", "start"]
