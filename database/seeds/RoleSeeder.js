"use strict";

/*
|--------------------------------------------------------------------------
| RoleSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use("Factory");

/** @type {import('adonis-acl/src/Models/Role')} */
const Role = use("Role");

class RoleSeeder {
  async run() {
    const roleAdmin = new Role();
    roleAdmin.name = "Admin";
    roleAdmin.slug = "admin";
    roleAdmin.description = "Admin of the system";
    await roleAdmin.save();

    const roleStudent = new Role();
    roleStudent.name = "Student";
    roleStudent.slug = "student";
    roleStudent.description = "Student of a specific course";
    await roleStudent.save();
  }
}

module.exports = RoleSeeder;
