"use strict";

/*
|--------------------------------------------------------------------------
| RoleSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

/** @type {import('@adonisjs/lucid/src/Factory')} */
const Factory = use("Factory");

/** @type {typeof import('../../app/Models/User')} */
const User = use("App/Models/User");

/** @type {typeof import('adonis-acl/src/Models/Role')} */
const Role = use("Adonis/Acl/Role");

/** @type {import('@adonisjs/lucid/src/Database')} */
const Database = use("Database");

class TeacherSeeder {
  async run() {
    const role = await Role.findByOrFail("slug", "admin");
    const admin = new User();
    admin.courseId = 0;
    admin.username = "teacher";
    admin.password = "iVL@teacher";
    await Database.transaction(async trx => {
      await admin.save(trx);
      await admin.roles().attach([role.id], null, trx);
    });
  }
}

module.exports = TeacherSeeder;
