'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class QuizzesSchema extends Schema {
  up() {
    this.table('quizzes', (table) => {
      // alter table
      table.boolean('startAnswerPhase').after('name').notNullable().defaultTo(0);
    })
  }

  down() {
    this.table('quizzes', (table) => {
      // reverse alternations
      table.dropColumn('startAnswerPhase');
    })
  }
}

module.exports = QuizzesSchema
