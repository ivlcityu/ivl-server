"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class UsersSchema extends Schema {
  up() {
    this.table("users", table => {
      // alter table
      table.timestamp("deleted_at").nullable();
    });
  }

  down() {
    this.table("users", table => {
      // reverse alternations
      table.dropColumn("deleted_at");
    });
  }
}

module.exports = UsersSchema;
