"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class AddQuestionIdToAnswersSchema extends Schema {
  up() {
    this.table("answers", table => {
      table
        .bigInteger("questionId")
        .unsigned()
        .after("id")
        .notNullable();
      table.index("questionId");
    });
  }

  down() {
    this.table("answers", table => {
      // reverse alternations
      table.dropIndex("questionId");
      table.dropColumn("questionId");
    });
  }
}

module.exports = AddQuestionIdToAnswersSchema;
