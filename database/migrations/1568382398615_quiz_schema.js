"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class QuizSchema extends Schema {
  up() {
    this.create("quizzes", table => {
      table.increments();
      table
        .bigInteger("courseId")
        .notNullable()
        .unsigned()
        .index();
      table.string("name", 200).notNullable();
      table.timestamp("start_at").notNullable();
      table.timestamp("end_at").notNullable();
      table.timestamps();
      table.timestamp("deleted_at");
    });
  }

  down() {
    this.drop("quizzes");
  }
}

module.exports = QuizSchema;
