"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class QuestionsSchema extends Schema {
  up() {
    this.create("questions", table => {
      table.increments();
      table
        .bigInteger("quizId")
        .unsigned()
        .notNullable();
      table
        .bigInteger("creatorId")
        .unsigned()
        .notNullable();
      table
        .integer("type")
        .unsigned()
        .notNullable();
      table.string("title", 400).notNullable();
      table.json("body").notNullable();
      table.timestamps();
      table.timestamp("deleted_at");
    });
  }

  down() {
    this.drop("questions");
  }
}

module.exports = QuestionsSchema;
