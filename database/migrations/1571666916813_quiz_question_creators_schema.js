"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class QuizQuestionCreatorsSchema extends Schema {
  up() {
    this.create("quiz_question_creators", table => {
      table.increments();
      table
        .bigInteger("quizId")
        .unsigned()
        .notNullable();
      table
        .bigInteger("userId")
        .unsigned()
        .notNullable();
      table.timestamps();
    });
  }

  down() {
    this.drop("quiz_question_creators");
  }
}

module.exports = QuizQuestionCreatorsSchema;
