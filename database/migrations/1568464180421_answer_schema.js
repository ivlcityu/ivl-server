"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class AnswerSchema extends Schema {
  up() {
    this.create("answers", table => {
      table.increments();
      table
        .bigInteger("userId")
        .unsigned()
        .notNullable();
      table.string("answer").notNullable();
      table.boolean("isCorrect").notNullable();
      table.timestamps();
    });
  }

  down() {
    this.drop("answers");
  }
}

module.exports = AnswerSchema;
