"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class AnswersSchema extends Schema {
  up() {
    this.table("answers", table => {
      // alter table
      table
        .bigInteger("quizId")
        .unsigned()
        .notNullable()
        .after("questionId");
    });
  }

  down() {
    this.table("answers", table => {
      // reverse alternations
      table.dropColumn("quizId");
    });
  }
}

module.exports = AnswersSchema;
