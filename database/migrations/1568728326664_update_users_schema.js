"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class UpdateUsersSchema extends Schema {
  up() {
    this.table("users", table => {
      // alter table
      table.dropUnique("username");
      table
        .string("identifier", 400)
        .after("username")
        .notNullable()
        .comment("username@courseId");
      table
        .bigInteger("courseId")
        .unsigned()
        .after("id");
      table.unique(["username", "courseId"]);
      table.unique("identifier");
    });
  }

  down() {
    this.table("users", table => {
      // reverse alternations
      table.dropUnique(["username", "courseId"]);
      table.dropUnique("identifier");
      table.dropColumn("courseId");
      table.dropColumn("identifier");
      table.unique("username");
    });
  }
}

module.exports = UpdateUsersSchema;
