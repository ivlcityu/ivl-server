"use strict";

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use("Schema");

class CoursesSchema extends Schema {
  up() {
    this.create("courses", table => {
      table.increments();
      table.string("code", 20).notNullable();
      table.string("name", 225).notNullable();
      table.timestamp("start_at");
      table.timestamp("end_at");
      table.timestamps();
      table.timestamp("deleted_at");
    });
  }

  down() {
    this.drop("courses");
  }
}

module.exports = CoursesSchema;
