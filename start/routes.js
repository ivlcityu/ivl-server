"use strict";

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use("Route");

Route.group(() => {
  Route.get("me", "MeController.index");
  Route.resource("courses", "CourseController").validator(
    new Map(
      Object.entries({
        "courses.store": "StoreCourse",
        "courses.update": "UpdateCourse",
        "courses.show": "ShowCourse",
        "courses.destroy": "DeleteCourse"
      })
    )
  );
  Route.post(
    "courses/:courseId/users/import",
    "UserController.import"
  ).validator("ImportUsers");
  Route.resource("courses/:courseId/users", "UserController").validator(
    new Map(
      Object.entries({
        "courses/:courseId/users.store": "StoreUser",
        "courses/:courseId/users.update": "UpdateUser",
        "courses/:courseId/users.index": "ListUsers",
        "courses/:courseId/users.destroy": "DeleteUser"
      })
    )
  );
  Route.resource("courses/:courseId/quizzes", "QuizController").validator(
    new Map(
      Object.entries({
        "courses/:courseId/quizzes.index": "ListQuizzes",
        "courses/:courseId/quizzes.store": "StoreQuiz",
        "courses/:courseId/quizzes.show": "ShowQuiz",
        "courses/:courseId/quizzes.update": "UpdateQuiz",
        "courses/:courseId/quizzes.destroy": "DeleteQuiz"
      })
    )
  );
  Route.patch(
    "courses/:courseId/quizzes/:id/answerState",
    "QuizController.updateAnswerPhase"
  ).validator("UpdateQuizAnswerPhase");
  Route.resource(
    "courses/:courseId/quizzes/:quizId/students",
    "QuizUserController"
  ).validator(
    new Map(
      Object.entries({
        "courses/:courseId/quizzes/:quizId/students.index": "ListQuizUsers",
        "courses/:courseId/quizzes/:quizId/students.store": "SyncQuizUsers"
      })
    )
  );
  Route.get(
    "courses/:courseId/report/quizzes",
    "ReportController.quizzes"
  ).validator("ShowAdminReport");
  Route.get(
    "report/quizzes/:quizId/students/:studentId",
    "ReportController.studentQuiz"
  ).validator("ShowAdminStudentQuizReport");
  Route.get(
    "courses/:courseId/report/students",
    "ReportController.students"
  ).validator("ShowAdminReport");
  Route.patch("admins/:id/resetPW", "UserController.resetPW").validator(
    "UpdateAdminPassword"
  );
})
  .prefix("api/admin")
  .namespace("Admin")
  .middleware(["auth:jwt", "is:(admin)"]);

Route.group(() => {
  Route.post("login", "UserController.login").validator("Login");
  Route.get("courses", "CourseController.index");
}).prefix("api");

Route.group(() => {
  Route.get("me", "MeController.index");
  Route.patch("me/password", "MeController.resetPassword").validator(
    "ResetPassword"
  );
  Route.post("refresh", "UserController.refresh").validator("RefreshToken");
  Route.get("courses/:courseId", "CourseController.show").validator(
    "UserShowCourse"
  );
  Route.get("courses/:courseId/report", "ReportController.course").validator(
    "UserCourseReport"
  );
  Route.resource("quizzes/:quizId/questions", "QuestionController").validator(
    new Map(
      Object.entries({
        "quizzes/:quizId/questions.index": "ListQuestions",
        "quizzes/:quizId/questions.destroy": "DeleteQuestion",
        "quizzes/:quizId/questions.store": "StoreQuestion",
        "quizzes/:quizId/questions.update": "UpdateQuestion"
      })
    )
  );
  Route.post("quizzes/:quizId/answers", "AnswerController.store").validator(
    "StoreAnswers"
  );
  Route.get("quizzes/:quizId/report", "ReportController.quiz").validator(
    "UserShowQuiz"
  );
  Route.get("quizzes/:quizId", "QuizController.show").validator("UserShowQuiz");
  Route.get(":courseId/quizzes", "QuizController.index").validator(
    "UserListQuizzes"
  );
  Route.post("files/uploadImage", "FileController.uploadImage").validator(
    "UploadImage"
  );
})
  .prefix("api")
  .middleware(["auth:jwt", "is:(student)"]);
