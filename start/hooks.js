const { hooks } = require("@adonisjs/ignitor");

function loadExistsRule() {
  const Validator = use("Validator");
  const Database = use("Database");

  const existsFn = async (data, field, message, args, get) => {
    const value = get(data, field);
    if (!value) {
      /**
       * skip validation if value is not defined. `required` rule
       * should take care of it.
       */
      return;
    }

    const [table, column] = args;
    const row = await Database.table(table)
      .where(column, value)
      .whereNull("deleted_at")
      .first();

    if (!row) {
      throw message;
    }
  };

  Validator.extend("exists", existsFn);
}

function loadNotExistsRule() {
  const Validator = use("Validator");
  const Database = use("Database");

  const notExistsFn = async (data, field, message, args, get) => {
    const value = get(data, field);
    if (!value) {
      return;
    }

    const [table, column, ignoreField, ignoreValue] = args;
    let query = Database.table(table)
      .where(column, value)
      .whereNull("deleted_at");
    if (ignoreField && ignoreValue) {
      query = query.where(ignoreField, "!=", ignoreValue);
    }
    const row = await query.first();

    if (row) {
      throw message;
    }
  };

  Validator.extend("notExists", notExistsFn);
}

function loadBelongToCourseRule() {
  const Validator = use("Validator");
  const Database = use("Database");

  const belongToCourseFn = async (data, field, message, args, get) => {
    const value = get(data, field);
    if (!value) {
      return;
    }

    const [table, courseId, courseIdColumn = "courseId"] = args;
    let query = Database.table(table)
      .where("id", value)
      .where(courseIdColumn, courseId)
      .whereNull("deleted_at");
    const row = await query.first();

    if (!row) {
      throw message;
    }
  };

  Validator.extend("belongToCourse", belongToCourseFn);
}

function loadBelongToQuizRule() {
  const Validator = use("Validator");
  const Database = use("Database");

  const belongToQuizFn = async (data, field, message, args, get) => {
    const value = get(data, field);
    if (!value) {
      return;
    }

    const [table, quizId, quizIdColumn = "quizId"] = args;
    let query = Database.table(table)
      .where("id", value)
      .where(quizIdColumn, quizId)
      .whereNull("deleted_at");
    const row = await query.first();

    if (!row) {
      throw message;
    }
  };

  Validator.extend("belongToQuiz", belongToQuizFn);
}

function loadExcludesRule() {
  const Validator = use("Validator");

  const excludesFn = async (data, field, message, args, get) => {
    const value = get(data, field);
    if (!value) {
      return;
    }

    const [subText] = args;
    const includesSubText = value.includes(subText);

    if (includesSubText) {
      throw message;
    }
  };

  Validator.extend("excludes", excludesFn);
}

function loadNoAnswersRule() {
  const Validator = use("Validator");
  const Database = use("Database");

  const noAnswersFn = async (data, field, message, args, get) => {
    const value = get(data, field);
    if (!value) {
      return;
    }

    const row = await Database.table("answers")
      .where("questionId", value)
      .first();

    if (row) {
      throw message;
    }
  };

  Validator.extend("noAnswers", noAnswersFn);
}

function loadNoAnswersToQuizRule() {
  const Validator = use("Validator");
  const Database = use("Database");

  const noAnswersFn = async (data, field, message, args, get) => {
    const value = get(data, field);
    if (!value) {
      return;
    }

    const [userId] = args;

    const row = await Database.table("answers")
      .where("quizId", value)
      .where("userId", userId)
      .first();

    if (row) {
      throw message;
    }
  };

  Validator.extend("noAnswersToQuiz", noAnswersFn);
}

hooks.after.providersBooted(() => {
  loadExistsRule();
  loadNotExistsRule();
  loadBelongToCourseRule();
  loadBelongToQuizRule();
  loadExcludesRule();
  loadNoAnswersRule();
  loadNoAnswersToQuizRule();
});
