"use strict";

class UploadImage {
  get validateAll() {
    return true;
  }

  get data() {
    const requestBody = this.ctx.request.all();
    const file = this.ctx.request.file("file");
    return {
      ...requestBody,
      file
    };
  }
  get rules() {
    return {
      file:
        "required|file|file_ext:png,jpg,jpeg|file_size:1.5mb|file_types:image"
    };
  }

  get messages() {
    return {
      "file.required": "Image is required",
      "file.file": "You should upload an jpg/png that is < 1.5MB",
      "file.fileExt": "You should upload an jpg/png that is < 1.5MB",
      "file.fileSize": "You should upload an jpg/png that is < 1.5MB",
      "file.fileTypes": "You should upload an jpg/png that is < 1.5MB"
    };
  }
}

module.exports = UploadImage;
