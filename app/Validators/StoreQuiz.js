"use strict";
const { rule } = require("indicative");
const parse = require("date-fns/parse");

class StoreQuiz {
  get validateAll() {
    return true;
  }

  get data() {
    const requestBody = this.ctx.request.all();
    const courseId = this.ctx.params.courseId;
    return {
      ...requestBody,
      courseId
    };
  }

  get rules() {
    const { start_at } = this.data;

    return {
      courseId: "required|exists:courses,id",
      name: "required|max:200",
      start_at: ["required", rule("dateFormat", "YYYY-MM-DD HH:mm:ss")],
      end_at: [
        "required",
        rule("dateFormat", "YYYY-MM-DD HH:mm:ss"),
        rule("after", parse(start_at, "yyyy-MM-dd HH:mm:ss", new Date()))
      ]
    };
  }

  get messages() {
    return {
      "courseId.exists": "Course does not exist",
      "name.required": "Quiz name is required",
      "name:max": "Quiz name is limited to 225 characters",
      "start_at.required": "Start time is required",
      "start_at.dateFormat":
        "Incorrect date format for start time, only accept YYYY-MM-DD HH:mm:ss",
      "end_at.required": "End time is required",
      "end_at.dateFormat":
        "Incorrect date format for end time, only accept YYYY-MM-DD HH:mm:ss",
      "end_at.after": "End time should be later than the start time"
    };
  }
}

module.exports = StoreQuiz;
