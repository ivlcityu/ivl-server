"use strict";

class ListUsers {
  get validateAll() {
    return true;
  }

  get data() {
    const requestBody = this.ctx.request.all();
    const courseId = this.ctx.params.courseId;
    return {
      ...requestBody,
      courseId
    };
  }

  get rules() {
    return {
      courseId: "required|exists:courses,id",
    };
  }

  get messages() {
    return {
      "courseId.exists": "Course does not exist",
    };
  }
}

module.exports = ListUsers;
