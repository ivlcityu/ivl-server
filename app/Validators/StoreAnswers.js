"use strict";

/** @type {typeof import('../Models/QuizQuestionCreator')} */
const Creator = use("App/Models/QuizQuestionCreator");

class StoreAnswers {
  get validateAll() {
    return true;
  }

  async authorize() {
    // only non question creator can answer question
    const { user } = this.ctx.auth;
    const isCreator = await Creator.isQuestionCreator(
      this.data.quizId,
      user.id
    );
    if (isCreator) {
      this.ctx.response.unauthorized("Not authorized");
    }
    return !isCreator;
  }

  get data() {
    const requestBody = this.ctx.request.all();
    const { quizId } = this.ctx.params;
    return {
      ...requestBody,
      quizId
    };
  }

  get rules() {
    const { quizId } = this.data;
    const { user } = this.ctx.auth;

    return {
      quizId: `required|exists:quizzes,id|noAnswersToQuiz:${user.id}`,
      "answers.*.questionId": `required|exists:questions,id|belongToQuiz:questions,${quizId}`,
      "answers.*.answer": "required"
    };
  }

  get messages() {
    return {
      "quizId.exists": "Quiz does not exist",
      "quizId.noAnswersToQuiz": "You have already answered the quiz",
      "answers.*.questionId.required": "Question does not exist",
      "answers.*.questionId.exists": "Question does not exist",
      "answers.*.questionId.belongToQuiz":
        "Question does not belong to the quiz",
      "answers.*.answer.required": "Answer should not be empty"
    };
  }
}

module.exports = StoreAnswers;
