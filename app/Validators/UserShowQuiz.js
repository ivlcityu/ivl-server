"use strict";

/** @type {typeof import('../../../Models/Quiz')} */
const Quiz = use("App/Models/Quiz");

class UserShowQuiz {
  async authorize() {
    const { user } = this.ctx.auth;
    const { quizId } = this.ctx.params;
    const quiz = await Quiz.findOrFail(quizId);
    const isCourseUser = `${quiz.courseId}` === `${user.courseId}`;
    if (!isCourseUser) {
      this.ctx.response.unauthorized("You are not in the course");
    }
    return isCourseUser;
  }

  get data() {
    const requestBody = this.ctx.request.all();
    const { quizId } = this.ctx.params;
    return {
      ...requestBody,
      quizId
    };
  }

  get rules() {
    return {
      quizId: "required|exists:quizzes,id"
    };
  }

  get messages() {
    return {
      "quizId.required": "Quiz id is required",
      "quizId.exists": "Quiz does not exist"
    };
  }
}

module.exports = UserShowQuiz;
