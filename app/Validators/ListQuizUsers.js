"use strict";

class ListQuizUsers {
  get validateAll() {
    return true;
  }

  get data() {
    const requestBody = this.ctx.request.all();
    const { courseId, quizId } = this.ctx.params;
    return {
      ...requestBody,
      courseId,
      quizId
    };
  }

  get rules() {
    const { courseId } = this.data;
    return {
      courseId: "required|exists:courses,id",
      quizId: `required|exists:quizzes,id|belongToCourse:quizzes,${courseId}`
    };
  }

  get messages() {
    return {
      "courseId.required": "Course Id is required",
      "courseId.exists": "Course does not exist",
      "quizId.exists": "Quiz does not exist",
      "quizId.belongToCourse": "Quiz does not belong to the course"
    };
  }
}

module.exports = ListQuizUsers;
