"use strict";
const { rule } = require("indicative");
const parse = require("date-fns/parse");

class UpdateCourse {
  get validateAll() {
    return true;
  }

  get data() {
    const requestBody = this.ctx.request.all();
    const id = this.ctx.params.id;
    return {
      ...requestBody,
      id
    };
  }

  get rules() {
    const id = this.ctx.params.id;
    const { start_at } = this.data;

    return {
      id: `required|exists:courses,id`,
      code: `required|max:20|not_exists:courses,code,id,${id}`,
      name: "required|max:225",
      start_at: ["required", rule("dateFormat", "YYYY-MM-DD HH:mm:ss")],
      end_at: [
        "required",
        rule("dateFormat", "YYYY-MM-DD HH:mm:ss"),
        rule("after", parse(start_at, "yyyy-MM-dd HH:mm:ss", new Date()))
      ]
    };
  }

  get messages() {
    return {
      "id.exists": "Course does not exist",
      "code.required": "Course code is required",
      "code.max": "Course code is limited to 20 characters",
      "code.notExists":
        "Course code is already taken by another course, please enter another code",
      "name.required": "Course name is required",
      "name:max": "Course name is limited to 225 characters",
      "start_at.required": "Start time is required",
      "start_at.dateFormat":
        "Incorrect date format for start time, only accept YYYY-MM-DD HH:mm:ss",
      "end_at.required": "End time is required",
      "end_at.dateFormat":
        "Incorrect date format for end time, only accept YYYY-MM-DD HH:mm:ss",
      "end_at.after": "End time should be later than the start time"
    };
  }
}

module.exports = UpdateCourse;
