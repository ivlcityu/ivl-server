"use strict";

class SyncQuizUsers {
  get validateAll() {
    return true;
  }

  get data() {
    const requestBody = this.ctx.request.all();
    const { courseId, quizId } = this.ctx.params;
    return {
      ...requestBody,
      courseId,
      quizId
    };
  }

  get rules() {
    const { courseId } = this.data;
    return {
      courseId: "required|exists:courses,id",
      quizId: `required|exists:quizzes,id|belongToCourse:quizzes,${courseId}`,
      userIds: "required|array",
      "userIds.*": `exists:users,id|belongToCourse:users,${courseId}`
    };
  }

  get messages() {
    return {
      "courseId.required": "Course Id is required",
      "courseId.exists": "Course does not exist",
      "quizId.exists": "Quiz does not exist",
      "quizId.belongToCourse": "Quiz does not belong to the course",
      "userIds.required": "List of users are required",
      "userIds.array": "List of users are required",
      "userIds.*.exists": "User does not exist in the course",
      "userIds.*.belongToCourse": "User does not belong to the course"
    };
  }
}

module.exports = SyncQuizUsers;
