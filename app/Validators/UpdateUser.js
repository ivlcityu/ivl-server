"use strict";

class UpdateUser {
  get validateAll() {
    return true;
  }

  get data() {
    const requestBody = this.ctx.request.all();
    const id = this.ctx.params.id;
    const courseId = this.ctx.params.courseId;
    return {
      ...requestBody,
      id,
      courseId
    };
  }
  get rules() {
    return {
      // validation rules
      courseId: "required|exists:courses,id",
      id: "required|exists:users,id",
      password: "required|min:8"
    };
  }

  get messages() {
    return {
      "courseId.required": "Course id is required",
      "courseId.exists": "Course does not exist",
      "id.required": "User id is required",
      "id.exists": "User does not exist",
      "password.required": "New password is required",
      "password.min": "New password should be at least 8 characters long"
    };
  }
}

module.exports = UpdateUser;
