"use strict";

const { rule } = require("indicative");

/** @type {typeof import('../Models/QuizQuestionCreator')} */
const Creator = use("App/Models/QuizQuestionCreator");

class StoreQuestion {
  get validateAll() {
    return true;
  }

  get sanitizationRules() {
    return {
      type: "trim"
    };
  }

  async authorize() {
    const { user } = this.ctx.auth;
    const isCreator = await Creator.isQuestionCreator(
      this.data.quizId,
      user.id
    );
    if (!isCreator) {
      this.ctx.response.unauthorized("Not authorized");
    }
    return !!isCreator;
  }

  get data() {
    const requestBody = this.ctx.request.all();
    const { quizId } = this.ctx.params;
    return {
      ...requestBody,
      quizId,
      formattedAnswer: (requestBody.answer || "").split(",")
    };
  }

  get rules() {
    const { options } = this.ctx.request.all();

    return {
      quizId: "required|exists:quizzes,id",
      type: ["required", rule("in", [0, 1, 2])],
      title: "required|max:400",
      options: "required|array|min:2",
      answer: "required",
      formattedAnswer: "required|array|min:1",
      "formattedAnswer.*": `integer|range:0,${options.length + 1}`,
      image: "required_when:type,1"
    };
  }

  get messages() {
    return {
      "quizId.exists": "Quiz does not exist",
      "type.required": "Question type is required",
      "type.in": "Question type is invalid",
      "title.required": "Title of the question is required",
      "title.max": "Title of the question is limited to 400 characters",
      "options.required": "At least 2 options are required for each question",
      "options.array": "At least 2 options are required for each question",
      "options.min": "At least 2 options are required for each question",
      "answer.required": "At lease 1 answer is required",
      "formattedAnswer.required": "At lease 1 answer is required",
      "formattedAnswer.array": "At lease 1 answer is required",
      "formattedAnswer.min": "At lease 1 answer is required",
      "formattedAnswer.*.integer": "The answer should be one of the options",
      "formattedAnswer.*.range": "The answer should be one of the options",
      "image.requiredWhen":
        "You need to supply one image for this question type"
    };
  }
}

module.exports = StoreQuestion;
