"use strict";

const { rule } = require("indicative");

class Login {
  get validateAll() {
    return true;
  }

  get rules() {
    return {
      courseId: "required|integer",
      username: ["required", rule("excludes", "|")],
      password: "required"
    };
  }

  get messages() {
    return {
      "courseId.required": "Course id is required",
      "username.required": "Username is required",
      "username.excludes": "Username should not include |",
      "password.required": "Password is required"
    };
  }
}

module.exports = Login;
