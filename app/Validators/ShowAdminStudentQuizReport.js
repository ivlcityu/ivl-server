"use strict";

class ShowAdminStudentQuizReport {
  get validateAll() {
    return true;
  }

  get data() {
    const requestBody = this.ctx.request.all();
    const courseId = this.ctx.params.courseId;
    const quizId = this.ctx.params.quizId;
    const studentId = this.ctx.params.studentId;
    return {
      ...requestBody,
      courseId,
      quizId,
      studentId,
    };
  }

  get rules() {
    return {
      quizId: "required|exists:quizzes,id",
      studentId: "required|exists:users,id",
    };
  }

  get messages() {
    return {
      "quizId.exists": "Quiz does not exist",
      "studentId.exists": "Student does not exist",
    };
  }
}

module.exports = ShowAdminStudentQuizReport;
