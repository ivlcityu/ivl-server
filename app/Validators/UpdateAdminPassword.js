"use strict";

class UpdateAdminPassword {
  get validateAll() {
    return true;
  }

  get data() {
    const requestBody = this.ctx.request.all();
    const id = this.ctx.params.id;
    return {
      ...requestBody,
      id
    };
  }

  get rules() {
    return {
      id: "required|exists:users,id",
      originalPW: "required",
      newPW: "required"
    };
  }

  get messages() {
    return {
      "id.exists": "User does not exist",
      "originalPW.required": "Original password is required",
      "newPW.required": "New password is required"
    };
  }
}

module.exports = UpdateAdminPassword;
