"use strict";

const { rule } = require("indicative");

/** @type {typeof import('../Models/QuizQuestionCreator')} */
const Creator = use("App/Models/QuizQuestionCreator");

class UpdateQuestion {
  get validateAll() {
    return true;
  }

  async authorize() {
    const { user } = this.ctx.auth;
    const isCreator = await Creator.isQuestionCreator(
      this.data.quizId,
      user.id
    );
    if (!isCreator) {
      this.ctx.response.unauthorized("Not authorized");
    }
    return !!isCreator;
  }

  get data() {
    const requestBody = this.ctx.request.all();
    const { quizId, id: questionId } = this.ctx.params;
    return {
      ...requestBody,
      quizId,
      questionId,
      formattedAnswer: (requestBody.answer || "").split(",")
    };
  }

  get rules() {
    const { options, quizId } = this.data;
    const { user } = this.ctx.auth;

    return {
      quizId: "required|exists:quizzes,id",
      creatorId: ['required', rule('in', [user.id]), 'exists:users,id'],
      questionId: `required|exists:questions,id|belongToQuiz:questions,${quizId}|noAnswers`,
      type: ["required", rule("in", [0, 1, 2])],
      title: "required|max:400",
      options: "required|array|min:2",
      answer: "required",
      formattedAnswer: "required|array|min:1",
      "formattedAnswer.*": `integer|range:0,${options.length + 1}`,
      image: "required_when:type,1"
    };
  }

  get messages() {
    return {
      "quizId.exists": "Quiz does not exist",
      "questionId.exists": "Question does not exist",
      "questionId.noAnswers":
        "You can not update question which already has answers",
      "creatorId.required": "Please provider creator id",
      "creatorId.in": "Who are you?",
      "creatorId.exists": "Creator does not exist",
      "type.required": "Question type is required",
      "type.in": "Question type is invalid",
      "title.required": "Title of the question is required",
      "title.max": "Title of the question is limited to 400 characters",
      "options.required": "At least 2 options are required for each question",
      "options.array": "At least 2 options are required for each question",
      "options.min": "At least 2 options are required for each question",
      "answer.required": "At lease 1 answer is required",
      "formattedAnswer.required": "At lease 1 answer is required",
      "formattedAnswer.array": "At lease 1 answer is required",
      "formattedAnswer.min": "At lease 1 answer is required",
      "formattedAnswer.*.integer": "The answer should be one of the options",
      "formattedAnswer.*.range": "The answer should be one of the options",
      "image.requiredWhen":
        "You need to supply one image for this question type",
      "image.url": "The image is invalid",
      "questionId.belongToQuiz": "Question does not belong to the quiz"
    };
  }
}

module.exports = UpdateQuestion;
