"use strict";

class StoreUser {
  get validateAll() {
    return true;
  }

  get data() {
    const requestBody = this.ctx.request.all();
    const courseId = this.ctx.params.courseId;
    return {
      ...requestBody,
      courseId
    };
  }

  get rules() {
    return {
      courseId: "required|exists:courses,id",
      username: "required|max:80",
      password: "required|min:8"
    };
  }

  get messages() {
    return {
      "courseId.exists": "Course does not exist",
      "username.required": "User name is required",
      "username:max": "User name is limited to 80 characters",
      "password.required": "Password is required",
      "password.min": "Password must be at least 8 characters"
    };
  }
}

module.exports = StoreUser;
