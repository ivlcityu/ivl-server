"use strict";

class ShowQuiz {
  get validateAll() {
    return true;
  }

  get data() {
    const requestBody = this.ctx.request.all();
    const { courseId, id } = this.ctx.params;
    return {
      ...requestBody,
      courseId,
      id
    };
  }

  get rules() {
    const { courseId } = this.data;

    return {
      courseId: "required|exists:courses,id",
      id: `required|exists:quizzes,id|belongToCourse:quizzes,${courseId}`
    };
  }

  get messages() {
    return {
      "courseId.required": "Course Id is required",
      "courseId.exists": "Course does not exist",
      "id.exists": "Quiz does not exist",
      "id.belongToCourse": "Quiz does not belong to the course"
    };
  }
}

module.exports = ShowQuiz;
