"use strict";

/** @type {typeof import('../Models/Quiz')} */
const Quiz = use("App/Models/Quiz");

class ListQuestions {
  async authorize() {
    const { user } = this.ctx.auth;
    const { quizId } = this.data;
    const quiz = await Quiz.find(quizId);
    const isCourseUser = `${user.courseId}` === `${quiz.courseId}`;
    if (!isCourseUser) {
      this.ctx.response.unauthorized("You are not in the course");
    }
    return isCourseUser;
  }

  get validateAll() {
    return true;
  }

  get data() {
    const requestBody = this.ctx.request.all();
    const { quizId } = this.ctx.params;
    return {
      ...requestBody,
      quizId
    };
  }

  get rules() {
    return {
      quizId: "required|exists:quizzes,id"
    };
  }

  get messages() {
    return {
      "quizId.exists": "Quiz does not exist"
    };
  }
}

module.exports = ListQuestions;
