"use strict";

class UpdateQuizAnswerPhase {
  get validateAll() {
    return true;
  }

  get data() {
    const requestBody = this.ctx.request.all();
    const { courseId, id } = this.ctx.params;
    return {
      ...requestBody,
      courseId,
      id
    };
  }

  get rules() {
    const { courseId } = this.data;
    return {
      courseId: "required|exists:courses,id",
      id: `required|exists:quizzes,id|belongToCourse:quizzes,${courseId}`,
      startPhase: "required|boolean"
    };
  }

  get messages() {
    return {
      "courseId.required": "Course Id is required",
      "courseId.exists": "Course does not exist",
      "id.exists": "Quiz does not exist",
      "id.belongToCourse": "Quiz does not belong to the course",
      "startPhase.exists": "Start Phase is required",
      "startPhase.boolean": "Start Phase should be 1/0 only"
    };
  }
}

module.exports = UpdateQuizAnswerPhase;
