"use strict";

class ShowCourse {
  get validateAll() {
    return true;
  }

  get data() {
    const requestBody = this.ctx.request.all();
    const id = this.ctx.params.id;
    return {
      ...requestBody,
      id
    };
  }

  get rules() {
    return {
      id: "required|exists:courses,id"
    };
  }

  get messages() {
    return {
      "id.exists": "Course does not exist"
    };
  }
}

module.exports = ShowCourse;
