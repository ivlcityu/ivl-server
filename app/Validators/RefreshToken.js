"use strict";

class RefreshToken {
  get rules() {
    return {
      refreshToken: "required"
    };
  }

  get messages() {
    return {
      refreshToken: "Refresh token is required"
    };
  }
}

module.exports = RefreshToken;
