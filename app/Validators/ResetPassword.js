"use strict";

/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use("Hash");

class ResetPassword {
  get validateAll() {
    return true;
  }

  async authorize() {
    const { user } = this.ctx.auth;
    const { originalPW } = this.ctx.request.all();
    const isSame = await Hash.verify(originalPW, user.password);
    if (!isSame) {
      this.ctx.response.unauthorized("Invalid original password.");
      return false;
    }
    return true;
  }

  get rules() {
    return {
      originalPW: "required",
      newPW: "required"
    };
  }

  get messages() {
    return {
      "originalPW.required": "Original password is required",
      "newPW.required": "New password is required"
    };
  }
}

module.exports = ResetPassword;
