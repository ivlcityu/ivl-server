"use strict";

class DeleteUser {
  get validateAll() {
    return true;
  }

  get data() {
    const requestBody = this.ctx.request.all();
    const courseId = this.ctx.params.courseId;
    const id = this.ctx.params.id;
    return {
      ...requestBody,
      id,
      courseId
    };
  }

  get rules() {
    return {
      id: "required|exists:users,id",
      courseId: "required|exists:courses,id",
    };
  }

  get messages() {
    return {
      "id.exists": "User does not exist",
      "courseId.exists": "Course does not exist"
    };
  }
}

module.exports = DeleteUser;
