"use strict";

/** @type {typeof import('../Models/QuizQuestionCreator')} */
const Creator = use("App/Models/QuizQuestionCreator");

class DeleteQuestion {
  get validateAll() {
    return true;
  }

  async authorize() {
    const { user } = this.ctx.auth;
    const isCreator = await Creator.isQuestionCreator(
      this.data.quizId,
      user.id
    );
    if (!isCreator) {
      this.ctx.response.unauthorized("Not authorized");
    }
    return !!isCreator;
  }

  get data() {
    const requestBody = this.ctx.request.all();
    const { quizId, id: questionId } = this.ctx.params;
    return {
      ...requestBody,
      quizId,
      questionId
    };
  }

  get rules() {
    const { quizId } = this.data;

    return {
      quizId: "required|exists:quizzes,id",
      questionId: `required|exists:questions,id|belongToQuiz:questions,${quizId}|noAnswers`
    };
  }

  get messages() {
    return {
      "quizId.exists": "Quiz does not exist",
      "questionId.exists": "Question does not exist",
      "questionId.belongToQuiz": "Question does not belong to the quiz",
      "questionId.noAnswers":
        "You can not update question which already has answers"
    };
  }
}

module.exports = DeleteQuestion;
