"use strict";
const { rule } = require("indicative");
const parse = require("date-fns/parse");

class StoreCourse {
  get validateAll() {
    return true;
  }

  get rules() {
    const { start_at } = this.ctx.request.all();

    return {
      code: "required|max:20|not_exists:courses,code",
      name: "required|max:225",
      start_at: ["required", rule("dateFormat", "YYYY-MM-DD HH:mm:ss")],
      end_at: [
        "required",
        rule("dateFormat", "YYYY-MM-DD HH:mm:ss"),
        rule("after", parse(start_at, "yyyy-MM-dd HH:mm:ss", new Date()))
      ]
    };
  }

  get messages() {
    return {
      "code.required": "Course code is required",
      "code.max": "Course code is limited to 20 characters",
      "code.not_exists":
        "Course code already exists, please enter another code",
      "name.required": "Course name is required",
      "name:max": "Course name is limited to 225 characters",
      "start_at.required": "Start time is required",
      "start_at.dateFormat":
        "Incorrect date format for start time, only accept YYYY-MM-DD HH:mm:ss",
      "end_at.required": "End time is required",
      "end_at.dateFormat":
        "Incorrect date format for end time, only accept YYYY-MM-DD HH:mm:ss",
      "end_at.after": "End time should be later than the start time"
    };
  }
}

module.exports = StoreCourse;
