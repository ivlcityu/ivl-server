"use strict";

class ImportUsers {
  get validateAll() {
    return true;
  }

  get data() {
    const requestBody = this.ctx.request.all();
    const courseId = this.ctx.params.courseId;
    const users = this.ctx.request.file("users");

    return {
      ...requestBody,
      users,
      courseId
    };
  }

  get rules() {
    return {
      courseId: "required|exists:courses,id",
      users: "required|file|file_ext:csv"
    };
  }

  get messages() {
    return {
      "courseId.exists": "Course does not exist",
      "users.required": "Required csv file for importing",
      "users.file": "The file should be a csv",
      "users.file_ext": "The file should be a csv"
    };
  }
}

module.exports = ImportUsers;
