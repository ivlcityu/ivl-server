"use strict";

class UserShowCourse {
  async authorize() {
    const { user } = this.ctx.auth;
    const { courseId } = this.ctx.params;
    const isCourseUser = `${user.courseId}` === `${courseId}`;
    if (!isCourseUser) {
      this.ctx.response.unauthorized("You are not in the course");
    }
    return isCourseUser;
  }

  get data() {
    const requestBody = this.ctx.request.all();
    const { courseId } = this.ctx.params;
    return {
      ...requestBody,
      courseId
    };
  }

  get rules() {
    return {
      courseId: "required|exists:courses,id"
    };
  }

  get messages() {
    return {
      "courseId.required": "Course id is required",
      "courseId.exists": "Course does not exist"
    };
  }
}

module.exports = UserShowCourse;
