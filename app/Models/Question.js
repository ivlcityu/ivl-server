"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");

class Question extends Model {
  static boot() {
    super.boot();
    this.addTrait("@provider:Jsonable");
    this.addTrait("@provider:Lucid/SoftDeletes");
  }

  static get hidden() {
    return ["deleted_at"];
  }

  static get table() {
    return "questions";
  }

  static get dates() {
    return super.dates.concat(["start_at", "end_at"]);
  }

  get jsonFields() {
    return ["body"];
  }

  static scopeOfQuiz(query, quizId) {
    return query.where("quizId", quizId);
  }

  answers() {
    return this.hasMany("App/Models/Answer", "id", "questionId");
  }
}

module.exports = Question;
