"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");

class Quiz extends Model {
  static boot() {
    super.boot();
    this.addTrait("@provider:Lucid/SoftDeletes");
  }

  static get table() {
    return "quizzes";
  }

  static get dates() {
    return super.dates.concat(["start_at", "end_at"]);
  }

  static scopeOfCourse(query, courseId) {
    return query.where("courseId", courseId);
  }

  static get hidden() {
    return ["deleted_at"];
  }

  answers() {
    return this.hasMany("App/Models/Answer", "id", "quizId");
  }

  questions() {
    return this.hasMany("App/Models/Question", "id", "quizId");
  }

  course() {
    return this.belongsTo("App/Models/Course", "courseId", "id");
  }

  questionCreators() {
    return this.hasMany("App/Models/QuizQuestionCreator", "id", "quizId");
  }
}

module.exports = Quiz;
