"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");

class QuizQuestionCreator extends Model {
  static get table() {
    return "quiz_question_creators";
  }

  static scopeOfQuiz(query, quizId) {
    return query.where("quizId", quizId);
  }

  static async isQuestionCreator(quizId, userId) {
    return !!(await this.query()
      .ofQuiz(quizId)
      .where("userId", userId)
      .first());
  }
}

module.exports = QuizQuestionCreator;
