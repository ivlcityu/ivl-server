"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");

class Course extends Model {
  static boot() {
    super.boot();

    this.addTrait("@provider:Lucid/SoftDeletes");
  }

  static get hidden() {
    return ["deleted_at"];
  }

  static get table() {
    return "courses";
  }

  static get dates() {
    return super.dates.concat(["start_at", "end_at"]);
  }

  static scopeLiveNow(query) {
    const now = new Date();
    return query.where("start_at", "<=", now).where("end_at", ">=", now);
  }

  users() {
    return this.hasMany("App/Models/User", "id", "courseId");
  }
}

module.exports = Course;
