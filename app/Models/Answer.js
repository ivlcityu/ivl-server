"use strict";

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");

class Answer extends Model {
  static get table() {
    return "answers";
  }

  static get dates() {
    return super.dates.concat(["start_at", "end_at"]);
  }

  question() {
    return this.belongsTo("App/Models/Question", "questionId", "id");
  }

  static scopeByUser(query, userId) {
    return query.where("userId", userId);
  }
}

module.exports = Answer;
