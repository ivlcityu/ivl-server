"use strict";

/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use("Hash");

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use("Model");

class User extends Model {
  static boot() {
    super.boot();

    /**
     * A hook to hash the user password before saving
     * it to the database.
     */
    this.addHook("beforeSave", async user => {
      if (user.dirty.password) {
        user.password = await Hash.make(user.password);
      }
      if (user.dirty.courseId || user.dirty.username) {
        user.identifier = User.generateIdentifier(
          user.dirty.courseId || user.courseId,
          user.dirty.username || user.username
        );
      }
    });

    this.addTrait("@provider:Lucid/SoftDeletes");
    this.addTrait("@provider:Adonis/Acl/HasRole");
    this.addTrait("@provider:Adonis/Acl/HasPermission");
  }

  static get hidden() {
    return ["password"];
  }

  /**
   * A relationship on tokens is required for auth to
   * work. Since features like `refreshTokens` or
   * `rememberToken` will be saved inside the
   * tokens table.
   *
   * @method tokens
   *
   * @return {Object}
   */
  tokens() {
    return this.hasMany("App/Models/Token");
  }

  course() {
    return this.hasOne("App/Models/Course", "courseId", "id");
  }

  static scopeOfCourse(query, courseId) {
    return query.where("courseId", courseId);
  }

  static generateIdentifier(courseId, username) {
    return `${username}|${courseId}`;
  }
}

module.exports = User;
