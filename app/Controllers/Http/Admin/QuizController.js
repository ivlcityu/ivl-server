"use strict";

/** @type {typeof import('../../../Models/Quiz')} */
const Quiz = use("App/Models/Quiz");

/** @type {typeof import('../../../Models/User')} */
const User = use("App/Models/User");

const _ = require("lodash");

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with quizzes as admin
 */
class QuizController {
  /**
   * Show a list of all quizzes.
   * GET :courseId/quizzes
   *
   * @param {object} ctx
   * @param {object} ctx.params
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ params, request, response, view }) {
    const { courseId } = params;
    return Quiz.query()
      .ofCourse(courseId)
      .with("questionCreators")
      .fetch();
  }

  /**
   * Render a form to be used for creating a new quiz.
   * GET :courseId/quizzes/create
   *
   * @param {object} ctx
   * @param {object} ctx.params
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ params, request, response, view }) {}

  /**
   * Create/save a new quiz.
   * POST :courseId/quizzes
   *
   * @param {object} ctx
   * @param {object} ctx.params
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ params, request, response }) {
    const { courseId } = params;
    const payload = request.only(["name", "start_at", "end_at"]);
    const quiz = new Quiz();
    quiz.merge({
      ...payload,
      courseId,
      startAnswerPhase: false
    });
    await quiz.save();
    return quiz;
  }

  /**
   * Display a single quiz.
   * GET :courseId/quizzes/:id
   *
   * @param {object} ctx
   * @param {object} ctx.params
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {
    const studentsCount = (
      await User.query()
        .where("courseId", params.courseId)
        .count()
    )[0]["count(*)"];
    const quiz = (
      await Quiz.query()
        .with("questions.answers")
        .where("id", params.id)
        .first()
    ).toJSON();
    quiz.questions = quiz.questions.map(question => {
      const { answers, ...output } = question;
      output.attempts = {
        total: answers.length,
        correct: _.filter(answers, {
          isCorrect: 1
        }).length
      };
      return output;
    });
    quiz.averageScore =
      studentsCount > 0
        ? _.sumBy(quiz.questions, "attempts.correct") / studentsCount
        : 0;
    return quiz;
  }

  /**
   * Render a form to update an existing quiz.
   * GET quizzes/:id/edit
   *
   * @param {object} ctx
   * @param {object} ctx.params
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {}

  /**
   * Update quiz details.
   * PUT or PATCH :courseId/quizzes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const { id: quizId } = params;
    const payload = request.only(["name", "start_at", "end_at"]);
    const quiz = await Quiz.findOrFail(quizId);
    quiz.merge({
      ...payload
    });
    await quiz.save();
    response.status(201);
  }

  /**
   * Update quiz answer state.
   * PUT or PATCH :courseId/quizzes/:id/answerState
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async updateAnswerPhase({ params, request, response }) {
    const { id: quizId } = params;
    const { startPhase } = request.only(["startPhase"]);
    const quiz = await Quiz.findOrFail(quizId);
    quiz.merge({
      startAnswerPhase: !!startPhase
    });
    await quiz.save();
    response.status(201);
  }

  /**
   * Delete a quiz with id.
   * DELETE :courseId/quizzes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
    const { id: quizId } = params;
    const quiz = await Quiz.findOrFail(quizId);
    await quiz.delete();
    response.status(204);
  }
}

module.exports = QuizController;
