"use strict";

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/** @type {typeof import('../../../Models/QuizQuestionCreator')} */
const Creator = use("App/Models/QuizQuestionCreator");

const _ = require("lodash");

/**
 * Resourceful controller for interacting with quizusers
 */
class QuizUserController {
  /**
   * Show a list of all quizusers.
   * GET :courseId/quizzes/:quizId/students
   *
   * @param {object} ctx
   * @param {object} ctx.params
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ params, request, response, view }) {
    return Creator.query()
      .ofQuiz(params.quizId)
      .fetch();
  }

  /**
   * Render a form to be used for creating a new quizuser.
   * GET :courseId/quizzes/:quizId/students/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {}

  /**
   * Sync question creators of quiz
   * POST :courseId/quizzes/:quizId/students
   *
   * @param {object} ctx
   * @param {object} ctx.params
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ params, request, response }) {
    const quizId = params.quizId;
    const { userIds } = request.only(["userIds"]);
    await Creator.query()
      .ofQuiz(params.quizId)
      .delete();
    _.uniq(_.filter(userIds)).forEach(userId => {
      const payload = { userId, quizId };
      Creator.create(payload);
    });
    response.status(204).send("");
  }

  /**
   * Display a single quizuser.
   * GET :courseId/quizzes/:quizId/students/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {}

  /**
   * Render a form to update an existing quizuser.
   * GET :courseId/quizzes/:quizId/students/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {}

  /**
   * Update quizuser details.
   * PUT or PATCH :courseId/quizzes/:quizId/students/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {}

  /**
   * Delete a quizuser with id.
   * DELETE :courseId/quizzes/:quizId/students/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {}
}

module.exports = QuizUserController;
