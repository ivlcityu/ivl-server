"use strict";

/** @type {typeof import('../../../Models/Quiz')} */
const Quiz = use("App/Models/Quiz");

/** @type {typeof import('../../../Models/User')} */
const User = use("App/Models/User");

/** @type {typeof import('../../../Models/Answer')} */
const Answer = use("App/Models/Answer");

const Database = use("Database");

const _ = require("lodash");

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

class ReportController {
  /**
   * Provide data to create performance report of quizzes
   * GET /courses/:courseId/report/quizzes
   *
   * @param {object} ctx
   * @param {object} ctx.params
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async quizzes({ params, request, response, view }) {
    const { courseId } = params;
    const quizzes = (await Quiz.query()
      .with("questions")
      .ofCourse(courseId)
      .fetch()).toJSON();
    const answers = await Database.from(Answer.table)
      .select("questionId")
      .count("* as totalAttempts")
      .sum("isCorrect as correctAttempts")
      .whereIn("quizId", _.map(quizzes, "id"))
      .groupBy("questionId");
    return {
      quizzes,
      answers: _.keyBy(answers, "questionId"),
    };
  }

  /**
   * Provide data to create performance report of students
   * GET /courses/:courseId/report/students
   *
   * @param {object} ctx
   * @param {object} ctx.params
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async students({ params, request, response, view }) {
    const { courseId } = params;
    const quizzes = (await Quiz.query()
      .with("questions")
      .ofCourse(courseId)
      .fetch()).toJSON();
    const answers = await Database.from(Answer.table)
      .select("userId", "quizId")
      .sum("isCorrect as correctAttempts")
      .whereIn("quizId", _.map(quizzes, "id"))
      .groupBy("userId", "quizId");
    return {
      quizzes,
      answers,
    };
  }


  /**
   * Provide data to create performance report of student in a quiz
   * GET /report/quizzes/:quizId/students/:studentId
   *
   * @param {object} ctx
   * @param {object} ctx.params
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async studentQuiz({ params, request, response, view }) {
    const { quizId, studentId } = params;
    const [quiz, student, report] = await Promise.all([
      Quiz.query().with('questions').where('id', quizId).first(),
      User.query().whereHas('roles', builder => {
        builder.where('slug', 'student')
      }).where('id', studentId).first(),
      Answer.query().where('quizId', quizId).byUser(studentId).fetch()
    ]);

    return {
      quiz,
      student,
      report
    }
  }
}

module.exports = ReportController;
