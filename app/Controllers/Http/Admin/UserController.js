"use strict";

/** @type {typeof import('../../../Models/User')} */
const User = use("App/Models/User");

/** @type {typeof import('../../../Models/Course')} */
const Course = use("App/Models/Course");

/** @type {import('@adonisjs/framework/src/Hash')} */
const Hash = use("Hash");

/** @type {import('@adonisjs/lucid/src/Database')} */
const Database = use("Database");

/** @type {typeof import('adonis-acl/src/Models/Role')} */
const Role = use("Adonis/Acl/Role");

const csv = require("csvtojson");
const _ = require("lodash");

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with users
 */
class UserController {
  /**
   * Show a list of all users as admin.
   * GET courses/:courseId/users
   *
   * @param {object} ctx
   * @param {object} ctx.params
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ params, request, response, view }) {
    const { courseId } = params;
    return User.query()
      .ofCourse(courseId)
      .fetch();
  }

  /**
   * Render a form to be used for creating a new user.
   * GET users/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {}

  /**
   * Create/save a new user.
   * POST courses/:courseId/users
   *
   * @param {object} ctx
   * @param {object} ctx.params
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ params, request, response }) {
    const { courseId } = params;
    const payload = request.only(["username", "password"]);
    const roleStudent = await Role.findByOrFail("slug", "student");
    const user = new User();
    user.merge({
      ...payload,
      courseId
    });

    await Database.transaction(async trx => {
      await user.save(trx);
      await user.roles().attach([roleStudent.id], null, trx);
    });
    return user;
  }

  /**
   * Create/save a new user.
   * POST courses/:courseId/users/import
   *
   * @param {object} ctx
   * @param {object} ctx.params
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async import({ params, request, response }) {
    const { courseId } = params;
    const file = request.file("users");

    const result = await csv().fromFile(file.tmpPath);
    const usersData = result.map(({ Username, Password }) => ({
      courseId,
      username: Username,
      password: Password
    }));
    const existedUserNames = (
      await User.query()
        .ofCourse(courseId)
        .select("username")
        .fetch()
    )
      .toJSON()
      .map(obj => obj.username);
    for (const user of usersData) {
      if (user.username.includes("|")) {
        throw new Error(`Username ${user.username} should not includes "|"`);
      }
      if (user.password.length === 0) {
        throw new Error(`User ${user.username} does not have a password`);
      }
      if (existedUserNames.includes(user.username)) {
        throw new Error(
          `User ${user.username} for the course is already in the system, please remove and upload again`
        );
      }
    }
    await Database.transaction(async trx => {
      const [users, roleStudent] = await Promise.all([
        User.createMany(usersData, trx),
        Role.findByOrFail("slug", "student")
      ]);
      for (let user of users) {
        await user.roles().attach([roleStudent.id], null, trx);
      }
    });

    return usersData;
  }

  /**
   * Display a single user.
   * GET users/:id
   *
   * @param {object} ctx
   * @param {object} ctx.params
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {}

  /**
   * Render a form to update an existing user.
   * GET users/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {}

  /**
   * Update user details (password only).
   * PUT or PATCH courses/:courseId/users/:id
   *
   * @param {object} ctx
   * @param {object} ctx.params
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const { id: userId } = params;
    const { password } = request.only(["password"]);
    const user = await User.find(userId);
    user.password = password;
    await user.save();
    return {
      password
    };
  }

  /**
   * Delete a user with id.
   * DELETE courses/:courseId/users/:id
   *
   * @param {object} ctx
   * @param {object} ctx.params
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
    (await User.find(params.id)).delete();
    response.status(204);
  }

  /**
   * Update admin password
   * PUT or PATCH /admin/:id/resetPW
   *
   * @param {object} ctx
   * @param {object} ctx.params
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async resetPW({ params, request, response }) {
    const { id: userId } = params;
    const { originalPW, newPW } = request.only(["originalPW", "newPW"]);
    const user = await User.find(userId);
    const isCorrectOldPW = await Hash.verify(originalPW, user.password);
    if (!isCorrectOldPW) {
      response.forbidden("");
      return;
    }
    user.password = newPW;
    await user.save();
    response.noContent();
  }
}

module.exports = UserController;
