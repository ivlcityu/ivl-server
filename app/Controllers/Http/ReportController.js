"use strict";

/** @type {typeof import('../../Models/Question')} */
const Question = use("App/Models/Question");
/** @type {typeof import('../../Models/Answer')} */
const Answer = use("App/Models/Answer");
/** @type {typeof import('../../Models/Quiz')} */
const Quiz = use("App/Models/Quiz");
/** @type {typeof import('../../Models/User')} */
const User = use("App/Models/User");

const Database = use("Database");

const _ = require("lodash");

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
/** @typedef {import('@adonisjs/auth/src/Schemes/Session')} AuthSession */

class ReportController {
  /**
   * Display a single report of the student.
   * GET quizzes/:quizId/report
   *
   * @param {object} ctx
   * @param {any} ctx.params
   * @param {AuthSession} ctx.auth
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async quiz({ auth, params, request, response, view }) {
    const { quizId } = params;
    const userId = auth.user.id;
    const subquery = Database.from(Question.table)
      .where("quizId", quizId)
      .select("id");
    const answers = (await Answer.query()
      .with("question")
      .whereIn("questionId", subquery)
      .where("userId", userId)
      .fetch()).toJSON();
    return answers.map(ans => {
      const { question } = ans;
      question.answer = ans.answer;
      question.isCorrect = !!ans.isCorrect;
      return question;
    });
  }

  /**
   * Display a single report of a course of the student.
   * GET courses/:courseId/report
   *
   * @param {object} ctx
   * @param {any} ctx.params
   * @param {AuthSession} ctx.auth
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async course({ auth, params, request, response, view }) {
    const { courseId } = params;
    const userId = auth.user.id;
    const studentsCount = (await User.query()
      .ofCourse(courseId)
      .count())[0]["count(*)"];
    const quizzes = (await Quiz.query()
      .withCount("questions")
      .ofCourse(courseId)
      .fetch()).toJSON();
    const quizIds = _.map(quizzes, "id");
    const stats = await Database.from(
      Database.from(Answer.table)
        .select("userId", "quizId")
        .sum("isCorrect as correctCount")
        .whereIn("quizId", quizIds)
        .groupBy("userId", "quizId")
        .as("rawStats")
    ).select(
      "userId",
      "quizId",
      "correctCount",
      Database.raw("RANK() OVER (PARTITION BY quizId ORDER BY correctCount DESC) `rank`")
    );
    const quizStats = _.groupBy(stats, "quizId");
    const quizPerformance = quizzes.map(quiz => {
      quiz.studentsCount = studentsCount;
      quiz.questionsCount = quiz.__meta__.questions_count;
      quiz.correctCount = _.get(_.find(quizStats[quiz.id], { userId }), "correctCount", 0);
      quiz.studentRank = _.get(_.find(quizStats[quiz.id], { userId }), "rank", "N/A");
      const { __meta__, ...response } = quiz;
      return response;
    });
    const cumulative = await Database.from(
      Database.from(Answer.table)
        .select("userId")
        .sum("isCorrect as correctCount")
        .whereIn("quizId", quizIds)
        .groupBy("userId")
        .as("rawStats")
    ).select("userId", Database.raw("RANK() OVER (ORDER BY correctCount DESC) `rank`"));
    return {
      quizzes: quizPerformance,
      cumulativeRank: _.get(_.find(cumulative, { userId }), "rank", "N/A"),
    };
  }
}

module.exports = ReportController;
