"use strict";

/** @type {typeof import('../../Models/Question')} */
const Question = use("App/Models/Question");

/** @type {typeof import('../../Models/Answer')} */
const Answer = use("App/Models/Answer");

const _ = require("lodash");

class AnswerController {
  /**
   * Create/save answers.
   * POST :quizId/answers
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ auth, params, request, response }) {
    const { quizId } = params;
    const { answers } = request.only(["answers"]);
    let questions = await Question.query()
      .ofQuiz(quizId)
      .fetch();
    questions = _.keyBy(questions.toJSON(), "id");
    const now = new Date();
    const payload = answers.map(ans => {
      const question = questions[ans.questionId];
      return {
        questionId: question.id,
        quizId: question.quizId,
        userId: auth.user.id,
        answer: ans.answer,
        isCorrect: ans.answer == question.body.answer,
        created_at: now,
        updated_at: now
      };
    });
    await Answer.query().insert(payload);
    response.status(201);
  }
}

module.exports = AnswerController;
