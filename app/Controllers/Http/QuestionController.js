"use strict";

/** @type {typeof import('../../Models/Question')} */
const Question = use("App/Models/Question");

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
/** @typedef {import('@adonisjs/auth/src/Schemes/Session')} AuthSession */

/**
 * Resourceful controller for interacting with questions as student
 */
class QuestionController {
  /**
   * Show a list of all questions.
   * GET :quizId/questions
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ params, request, response, view }) {
    const { quizId } = params;
    return Question.query()
      .ofQuiz(quizId)
      .fetch();
  }

  /**
   * Render a form to be used for creating a new question.
   * GET questions/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({ request, response, view }) {}

  /**
   * Create/save a new question.
   * POST :quizId/questions
   *
   * @param {object} ctx
   * @param {object} ctx.params
   * @param {AuthSession} ctx.auth
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({ auth, params, request, response }) {
    const { quizId } = params;
    const payload = request.only([
      "type",
      "title",
      "options",
      "answer",
      "image"
    ]);
    const question = new Question();
    question.quizId = quizId;
    question.creatorId = auth.user.id;
    question.type = payload.type;
    question.title = payload.title;
    question.body = {
      options: payload.options,
      answer: payload.answer
    };
    if (payload.image) {
      question.body.image = payload.image;
    }
    await question.save();
    response.status(201);
  }

  /**
   * Display a single question.
   * GET questions/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({ params, request, response, view }) {}

  /**
   * Render a form to update an existing question.
   * GET questions/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({ params, request, response, view }) {}

  /**
   * Update question details.
   * PUT or PATCH :quizId/questions/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({ params, request, response }) {
    const { quizId, id: questionId } = params;
    const payload = request.only([
      "creatorId",
      "type",
      "title",
      "options",
      "answer",
      "image"
    ]);
    const question = await Question.findOrFail(questionId);
    question.quizId = quizId;
    question.creatorId = payload.creatorId;
    question.type = payload.type;
    question.title = payload.title;
    question.body = {
      options: payload.options,
      answer: payload.answer
    };
    if (payload.image) {
      question.body.image = payload.image;
    }
    await question.save();
    response.status(201);
  }

  /**
   * Delete a question with id.
   * DELETE :quizId/questions/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({ params, request, response }) {
    const { quizId, id: questionId } = params;
    const question = await Question.query()
      .where("id", questionId)
      .where("quizId", quizId)
      .firstOrFail();
    await question.delete();
    response.status(204);
  }
}

module.exports = QuestionController;
