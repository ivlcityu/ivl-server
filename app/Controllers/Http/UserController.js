"use strict";

/** @type {typeof import('../../Models/User')} */
const User = use("App/Models/User");

const Hash = use("Hash");

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

class UserController {
  /**
   * Log user by courseId, username and password
   * GET login
   *
   * @param {object} ctx
   * @param {object} ctx.auth
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async login({ request, response, view, auth }) {
    const { courseId, username, password } = request.only([
      "courseId",
      "username",
      "password"
    ]);
    const identifier = User.generateIdentifier(courseId, username);
    return auth.withRefreshToken().attempt(identifier, password);
  }

  /**
   * Refresh JWT token and refresh token by refresh token
   * GET refresh
   *
   * @param {object} ctx
   * @param {object} ctx.auth
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async refresh({ request, response, view, auth }) {
    const refreshToken = request.input("refreshToken");
    return auth.newRefreshToken().generateForRefreshToken(refreshToken);
  }
}

module.exports = UserController;
