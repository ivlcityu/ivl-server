"use strict";

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
/** @typedef {import('@adonisjs/auth/src/Schemes/Session')} AuthSession */

class MeController {
  /**
   * Show current user.
   * GET /me
   *
   * @param {object} ctx
   * @param {AuthSession} ctx.auth
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({ auth, request, response, view }) {
    const user = auth.user;
    const course = await user.course().fetch();

    return {
      id: user.id,
      username: user.username,
      course: {
        courseId: user.courseId,
        code: course.code,
        name: course.name,
        start_at: course.start_at,
        end_at: course.end_at
      }
    };
  }

  /**
   * Update current user password.
   * PATCH /me/password
   *
   * @param {object} ctx
   * @param {AuthSession} ctx.auth
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async resetPassword({ auth, request, response }) {
    const user = auth.user;
    const { newPW } = request.only(["newPW"]);
    user.password = newPW;
    await user.save();
    response.status(204);
  }
}

module.exports = MeController;
