"use strict";

const Helpers = use("Helpers");
const Drive = use("Drive");
const Env = use("Env");
const uuid = require("uuid/v4");
const sharp = require("sharp");
const path = require("path");

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

class FileController {
  /**
   * Upload images to system
   * POST images
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async uploadImage({ request, response, view }) {
    const file = request.file("file", { types: ["image"] });
    const destFileName = await this.getFileName(
      Helpers.publicPath("images"),
      file.extname
    );
    await sharp(file.tmpPath)
      .resize(600, 600, { fit: "inside" })
      .toFile(path.join(Helpers.publicPath("images"), destFileName));
    return {
      url: new URL(`/images/${destFileName}`, Env.get("APP_URL")).href
    };
  }

  async getFileName(folderPath, ext) {
    let fileName = "";
    let exists = false;
    do {
      fileName = `${uuid()}.${ext}`;
      exists = await Drive.exists(`${folderPath}/${fileName}`);
    } while (exists);
    return fileName;
  }
}

module.exports = FileController;
