"use strict";

/** @type {typeof import('../../Models/Quiz')} */
const Quiz = use("App/Models/Quiz");

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with quizzes
 */
class QuizController {
  /**
   * Show a list of all quizzes for students.
   * GET :courseId/quizzes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index({
    params,
    request,
    response,
    view
  }) {
    const {
      courseId
    } = params;
    const quizzes = await Quiz.query()
      .ofCourse(courseId)
      .fetch();
    return quizzes.toJSON();
  }

  /**
   * Render a form to be used for creating a new quiz.
   * GET quizzes/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create({
    request,
    response,
    view
  }) {}

  /**
   * Create/save a new quiz.
   * POST quizzes
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store({
    request,
    response
  }) {}

  /**
   * Display a single quiz.
   * GET quizzes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({
    auth,
    params,
    request,
    response,
    view
  }) {
    const userId = auth.user.id;
    const quiz = await Quiz.query()
      .withCount('questions')
      .with("questionCreators", (builder) => {
        builder.where('userId', userId)
      })
      .with("answers", (builder) => {
        builder.where('userId', userId)
      })
      .where('id', params.quizId)
      .first();
    return {
      id: quiz.id,
      courseId: quiz.courseId,
      name: quiz.name,
      start_at: quiz.start_at,
      end_at: quiz.end_at,
      questions_count: quiz.questions_count,
      startAnswerPhase: quiz.startAnswerPhase,
      isCompleted: quiz.getRelated('answers').rows.length > 0,
      isCreator: quiz.getRelated('questionCreators').rows.length === 1,
    };
  }

  /**
   * Render a form to update an existing quiz.
   * GET quizzes/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit({
    params,
    request,
    response,
    view
  }) {}

  /**
   * Update quiz details.
   * PUT or PATCH quizzes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update({
    params,
    request,
    response
  }) {}

  /**
   * Delete a quiz with id.
   * DELETE quizzes/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy({
    params,
    request,
    response
  }) {}
}

module.exports = QuizController;
