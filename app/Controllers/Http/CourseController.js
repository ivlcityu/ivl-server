"use strict";

/** @type {typeof import('../../Models/Course')} */
const Course = use("App/Models/Course");

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */
/** @typedef {import('@adonisjs/auth/src/Schemes/Session')} AuthSession */

class CourseController {
  /**
   * Show a list of all courses.
   * GET courses
   *
   * @param {object} ctx
   * @param {AuthSession} ctx.auth
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  index({
    auth,
    request,
    response,
    view
  }) {
    return Course.query()
      .liveNow()
      .fetch();
  }

  /**
   * Display a single course.
   * GET courses/:courseId
   *
   * @param {object} ctx
   * @param {object} ctx.params
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show({
    params,
    request,
    response,
    view
  }) {
    const {
      __meta__,
      ...course
    } = (await Course.query().withCount('users').where('id', params.courseId).first()).toJSON();
    return {
      ...course,
      studentsCount: __meta__.users_count
    }
  }
}

module.exports = CourseController;
