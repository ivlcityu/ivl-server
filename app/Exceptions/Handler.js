"use strict";

const BaseExceptionHandler = use("BaseExceptionHandler");

/**
 * This class handles all exceptions thrown during
 * the HTTP request lifecycle.
 *
 * @class ExceptionHandler
 */
class ExceptionHandler extends BaseExceptionHandler {
  /**
   * Handle exception thrown during the HTTP lifecycle
   *
   * @method handle
   *
   * @param  {Object} error
   * @param  {Object} options.request
   * @param  {Object} options.response
   *
   * @return {void}
   */
  async handle(error, { request, response }) {
    if (error.name === "ValidationException") {
      response.status(error.status).send(error.messages);
      return;
    }
    if (error.name === "InvalidJwtToken") {
      response
        .status(error.status)
        .send("Invalid login token, please logout and login and try again");
      return;
    }
    response.status(error.status).send([{ message: error.message }]);
  }

  /**
   * Report exception for logging or debugging.
   *
   * @method report
   *
   * @param  {Object} error
   * @param  {Object} options.request
   *
   * @return {void}
   */
  async report(error, { request }) {
    console.error(error);
  }
}

module.exports = ExceptionHandler;
